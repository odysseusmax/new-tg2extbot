import logging

from .config import Config


logging.basicConfig(level=logging.DEBUG if Config.DEBUG else logging.INFO)
logging.getLogger("pyrogram").setLevel(
    logging.INFO if Config.DEBUG else logging.WARNING
)
# logging.getLogger("aiohttp").setLevel(logging.INFO if Config.DEBUG else logging.WARNING)
