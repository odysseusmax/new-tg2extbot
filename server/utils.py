import asyncio
import json
import logging
import datetime
from hashlib import blake2b
from base64 import b64decode
from dataclasses import dataclass
from collections import defaultdict
import time
from typing import Tuple, Union

from aiohttp.web import Request

from .config import Config


logger = logging.getLogger(__name__)


@dataclass
class FileDetails:
    file_id: str
    file_size: int
    file_name: str
    mime_type: str


class Utilities:
    @staticmethod
    def get_human_size(num: int, split=False) -> Union[str, Tuple[int, str]]:
        base = 1024.0
        sufix_list = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
        for unit in sufix_list:
            if abs(num) < base:
                if split:
                    return round(num, 2), unit

                return f"{round(num, 2)} {unit}"
            num /= base

    @staticmethod
    def calc_hash(req: Request) -> str:
        h = blake2b(key=b"asdfghjklmnbvcxzqwertyuiop", digest_size=16)
        from_ip = Utilities.get_requester_ip(req)
        h.update(from_ip.encode())
        return h.hexdigest()

    @staticmethod
    def get_requester_ip(req: Request) -> str:
        forwarded_for = req.headers.get("X-Forwarded-For")
        if forwarded_for:
            from_ip = forwarded_for.split(",")[-1].strip()
        else:
            from_ip = req.remote
        return from_ip

    @staticmethod
    def parse_data(data_string: str) -> FileDetails:
        decoded_data_str = b64decode(data_string).decode()
        data: dict = json.loads(decoded_data_str)
        return FileDetails(**data)


class _Limitter:
    def __init__(self, ongoing_requests, ip, file_id):
        self.ongoing_requests = ongoing_requests
        self.ip = ip
        self.file_id = file_id
        self.current_size = 0
        self.start_time = time.time()

    def __enter__(self):
        self.ongoing_requests[self.file_id][self.ip] += 1

    def __exit__(self, exc_type, exc_value, traceback):
        self.ongoing_requests[self.file_id][self.ip] -= 1

    async def check_speed(self, chunk_size: int):
        self.current_size += chunk_size
        no_of_connections = self.ongoing_requests[self.file_id][self.ip]
        max_speed = Config.PER_IP_SPEED / no_of_connections

        while True:
            time_diff = time.time() - self.start_time
            speed, unit = 0, "B"
            if time_diff > 1:
                speed, unit = Utilities.get_human_size(
                    self.current_size / time_diff, True
                )
                if unit == "MB" and speed > max_speed:
                    await asyncio.sleep(1)
                    continue
            break

        # print(self.current_size, max_speed, speed, unit)


class ConnectionLimitter:
    def __init__(self):
        self.ongoing_requests = defaultdict(lambda: defaultdict(lambda: 0))

    def limit(self, ip: str, file_id: int):
        return _Limitter(self.ongoing_requests, ip, file_id)

    def get_connection_count(self, ip: str, file_id: int):
        return self.ongoing_requests[file_id][ip]
