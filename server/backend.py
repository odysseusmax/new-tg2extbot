import math
from typing import Generator, Union

from pyrogram import Client
from pyrogram.types import (
    Message,
    Audio,
    Document,
    Photo,
    Sticker,
    Animation,
    Video,
    Voice,
    VideoNote,
)


Media = Union[Animation, Video, VideoNote, Audio, Document, Photo, Sticker, Voice]


def get_media(message: Message) -> Media:
    available_media = (
        "audio",
        "document",
        "photo",
        "sticker",
        "animation",
        "video",
        "voice",
        "video_note",
        "new_chat_photo",
    )
    for kind in available_media:
        media = getattr(message, kind, None)
        if media is not None:
            return media
    else:
        raise ValueError("This message doesn't contain any downloadable media")


async def stream_media(
    client: Client, file_id: str, offset_bytes: int = 0, limit_bytes: int = None
) -> Generator[bytes, None, None]:
    chunk_size = 1024 * 1024

    offset_chunk = int(offset_bytes // chunk_size)
    offset_chunk_cutof = int(offset_bytes % chunk_size)

    limit_chunk = math.ceil(limit_bytes / chunk_size)
    limit_chunk_cutof = int(limit_bytes % chunk_size)

    bytes_generator = client.stream_media(
        message=file_id, limit=limit_chunk, offset=offset_chunk
    )
    count = offset_chunk
    async for chunk in bytes_generator:
        chunk_ = chunk
        if count == offset_chunk:
            chunk_ = chunk[offset_chunk_cutof:]
        elif count == limit_chunk:
            chunk_ = chunk[:limit_chunk_cutof]

        yield chunk_
        count += 1
