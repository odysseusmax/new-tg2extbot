import sys
import asyncio

from aiohttp import web

from .server import init_server
from .config import Config

if sys.platform.startswith("linux"):
    try:
        import uvloop

        uvloop.install()
    except ImportError:
        pass


loop = asyncio.new_event_loop()
asyncio.set_event_loop(loop)
app = loop.run_until_complete(init_server())
web.run_app(app, host=Config.HOST, port=Config.PORT, loop=loop)
