import os
import pathlib
import asyncio

import jinja2
import aiohttp_jinja2
from aiohttp import web
from pyrogram import Client

from .routes import routes
from .config import Config


def setup_jinja(app: web.Application):
    TEMPLATES_ROOT = pathlib.Path(__file__).parent / "templates"
    loader = jinja2.FileSystemLoader(str(TEMPLATES_ROOT))
    aiohttp_jinja2.setup(app, loader=loader)


async def setup_telegram(app: web.Application):
    client = Client(
        "bot",
        api_id=Config.API_ID,
        api_hash=Config.API_HASH,
        bot_token=Config.BOT_TOKEN,
        in_memory=True,
    )
    await client.start()

    async def close_tg(app):
        await client.stop()

    app.on_shutdown.append(close_tg)
    app["tg_client"] = client


async def init_server():
    server = web.Application()
    await setup_telegram(server)
    setup_jinja(server)
    server.add_routes(routes)
    os.makedirs("thumbs", exist_ok=True)
    return server


if __name__ == "__main__":
    app = asyncio.get_event_loop().run_until_complete(init_server())
    web.run_app(app, host=Config.HOST, port=Config.PORT)
