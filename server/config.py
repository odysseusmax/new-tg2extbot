import os


class Config:
    API_ID = os.getenv("API_ID")
    API_HASH = os.getenv("API_HASH")
    BOT_TOKEN = os.getenv("BOT_TOKEN")

    HOST = os.getenv("HOST", "0.0.0.0")
    PORT = os.getenv("PORT", "5678")

    DEBUG = bool(os.getenv("DEBUG"))

    LINK_EXPIRY = int(os.getenv("EXPIRY", "1"))
    PER_IP_SPEED = int(os.getenv("PER_IP_SPEED", "1"))  # MB
