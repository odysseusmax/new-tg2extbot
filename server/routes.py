import json
import logging
from base64 import b64encode
from mimetypes import guess_extension
import re

from aiohttp import web
from pyrogram import Client

from .backend import stream_media
from .utils import Utilities


routes = web.RouteTableDef()
logger = logging.getLogger(__name__)


@routes.post("/callback", name="callback")
async def bot_callback(req: web.Request):
    update: dict = await req.json()
    message: dict = update.get("message")
    if not message:
        return web.json_response({"ok": True})

    allowed_media = ("video", "audio", "document", "photo")
    chat: dict = message.get("chat")
    if not chat or chat.get("type", "") != "private":
        return web.json_response({"ok": True})

    text = message.get("text", "")
    if text == "/start":
        return web.json_response(
            {
                "text": "Hi buddy!\n\nForward any file to get the download link.",
                "chat_id": chat.get("id"),
                "method": "sendMessage",
                "reply_to_message_id": message.get("message_id"),
            }
        )

    media = None
    for typ in allowed_media:
        if typ in message:
            media = message.get(typ)
            break

    if media is None:
        return web.json_response({"ok": True})

    if isinstance(media, list):
        media = sorted(media, key=lambda x: x["file_size"])[0]

    mime_type = media.get("mime_type", "application/octet-stream")
    file_name = media.get("file_name")
    if not file_name:
        file_name = "tg-file"
        ext = guess_extension(mime_type) or ".bin"
        file_name += ext

    file_name = re.sub(r"\s", "", file_name)
    data = {
        "file_id": media["file_id"],
        "file_size": media["file_size"],
        "file_name": file_name,
        "mime_type": mime_type,
    }
    encoded = b64encode(json.dumps(data).encode()).decode()
    url = req.url.with_path(f"/download/{encoded}/{file_name}")
    return web.json_response(
        {
            "text": f'Here\'s your download URL\n\n<a href="{url}">Download</a>',
            "chat_id": chat.get("id"),
            "parse_mode": "HTML",
            "reply_to_message_id": message.get("message_id"),
            "allow_sending_without_reply": True,
            "method": "sendMessage",
        }
    )


@routes.head("/download/{data}/{file_name:.*}", name="download-head")
async def head(req: web.Request):
    return await stream(req, head=True)


@routes.get("/download/{data}/{file_name:.*}", name="download")
async def get(req: web.Request):
    return await stream(req)


@routes.get(r"/{x:.*}", name="home")
async def index(req: web.Request):
    raise web.HTTPFound("https://tx.me/tg2extbot")


async def stream(req: web.Request, head=False):
    raw_data = req.match_info["data"]
    data = Utilities.parse_data(raw_data)

    file_id = data.file_id
    file_name = data.file_name
    client: Client = req.app["tg_client"]

    size = data.file_size
    try:
        offset = req.http_range.start or 0
        limit = min(req.http_range.stop or size, size)
        partial = True if (offset or limit != size) else False
        if (limit > size) or (offset < 0) or (limit < offset):
            raise ValueError("range not in acceptable format")
    except ValueError:
        return web.Response(
            status=416,
            text="416: Range Not Satisfiable",
            headers={"Content-Range": f"bytes */{size}"},
        )

    mime_type = data.mime_type
    headers = {
        "Content-Type": mime_type,
        "Content-Length": f"{limit - offset}",
        "Content-Range": f"bytes {offset}-{limit}/{size}",
        "Accept-Ranges": "bytes",
        "Content-Disposition": f'attachment; filename="{file_name}"',
    }
    status = 206 if partial else 200
    if head:
        return web.Response(status=status, headers=headers)

    logger.info("Serving file %s Range %s - %s", file_id, offset, limit)
    body = stream_media(client, file_id, offset_bytes=offset, limit_bytes=limit)
    return web.Response(status=status, headers=headers, body=body)
